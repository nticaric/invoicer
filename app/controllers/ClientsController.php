<?php

class ClientsController extends BaseController {

    public function index()
    {
        return View::make('clients.index');
    }

    public function getClients()
    {
        return Client::all();
    }

    public function add()
    {
        return Client::create(Input::all());
    }
    

}