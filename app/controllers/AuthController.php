<?php

class AuthController extends BaseController {

    public function showLogin()
    {
        return View::make('login');
    }

    public function login()
    {
        $user = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        );
        if (Auth::attempt($user, Input::get('rememberme', null))) {
            return Redirect::intended('/');
        }

        // authentication failure! lets go back to the login page
        return Redirect::to('login')
            ->withErrors('Your username/password combination was incorrect.')
            ->withInput();
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/')->with('message', 'You have been logged out from the system');
    }
    
    public function loginWithGoogle() {

        // get data from input
        $code = Input::get( 'code' );

        // get google service
        $googleService = OAuth::consumer( 'Google' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken( $code );

            // Send a request with it
            $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );

            $profile = Profile::whereUid($result['id'])->first();
            

            if (empty($profile)) {

                $user = new User;
                $user->firstname = $result['given_name'];
                $user->lastname  = $result['family_name'];
                $user->email     = $result['email'];
                $user->avatar    = $result['picture'];
                $user->save();

                $profile = new Profile();
                $profile->uid = $result['id'];
                $profile = $user->profiles()->save($profile);
            }

            $profile->access_token = $token->getAccessToken();
            $profile->save();
         
            $user = $profile->user;
         
            Auth::login($user);
            
            return Redirect::to('/')->with('message', 'Logged in with Facebook');
        }
        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to google login url
            return Redirect::to( (string)$url );
        }
    }

    public function loginWithFacebook() {

        // get data from input
        $code = Input::get( 'code' );

        // get fb service
        $fb = OAuth::consumer( 'Facebook' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken( $code );

            // Send a request with it
            $result = json_decode( $fb->request( '/me' ), true );

            $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
            echo $message. "<br/>";

            //Var_dump
            //display whole array().
            dd($result);

        }
        // if not ask for permission first
        else {
            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
             return Redirect::to( (string)$url );
        }

    }
}