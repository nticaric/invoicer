<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ["before" => "auth", "uses" => 'DashboardController@index']);

Route::get('login', 'AuthController@showLogin');
Route::get('logout', 'AuthController@logout');
Route::post('login', 'AuthController@login');
Route::get('login/google', 'AuthController@loginWithGoogle');
Route::get('login/facebook', 'AuthController@loginWithFacebook');

Route::group(array('before' => 'auth'), function()
{
    Route::get('invoices/new', 'InvoiceController@newInvoice');
    Route::get('clients', 'ClientsController@index');
    Route::get('estimates', 'InvoiceController@newInvoice');
    Route::get('expenses', 'InvoiceController@newInvoice');
    Route::get('timetracking', 'InvoiceController@newInvoice');
    Route::get('reports', 'InvoiceController@newInvoice');

    Route::get('clients.json', 'ClientsController@getClients');
    Route::post('clients/add', 'ClientsController@add');

});