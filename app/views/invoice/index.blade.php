@extends('layouts.main')

@section('container')
<div class="row">
        <div class="col-md-12 col-lg-11 center-column">
          <div class="panel invoice-panel">
            <div class="panel-heading panel-visible">
              <div class="panel-title"> <span class="glyphicon glyphicon-print"></span> Printable Invoice</div>
              <div class="panel-btns pull-right margin-left">
                <button type="button" class="btn btn-default btn-gradient margin-right-sm"> <i class="fa fa-plus-square padding-right-sm"></i> New Invoice</button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-gradient dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-cogwheel"></span></button>
                  <ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
                    <li><a><i class="fa fa-user"></i> View Profile </a></li>
                    <li><a><i class="fa fa-envelope-o"></i> Message </a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel-sidemenu">
              <ul class="nav">
                <li class="nav-title">Invoice Menu</li>
                <li><a href="" data-toggle="tab"><i class="fa fa-print"></i> Print</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-envelope-o"></i> Email Item</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-tablet"></i> Export PDF</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-file"></i> Calculator</a></li>
                <li class="nav-title">Item Options</li>
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-pencil"></i> Edit</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-trash-o"></i> Delete</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-minus-square-o"></i> Void</a></li>
                <li><a href="" data-toggle="tab"><i class="fa fa-refresh"></i> Clone</a></li>
                <li class="nav-title">User Payments</li>
                <li><a href="#tab2" data-toggle="tab"><i class="fa fa-usd"></i> Payments</a></li>
                <li><a href="#tab3" data-toggle="tab"><i class="fa fa-ticket"></i> Credit</a></li>
              </ul>
            </div>
            <div class="panel-body" id="invoice-item">
              <div class="row" id="invoice-title">
                <div class="col-md-4">
                  <div class="pull-left">
                    <h1> INVOICE </h1>
                    <h5 class="margin-none"> Created: Nov 23 2013 </h5>
                    <h5 class="margin-none"> Status: <b class="text-danger">Unpaid - Late</b> </h5>
                  </div>
                </div>
                <div class="col-md-4 text-center">
                  <div class="invoice-logo hidden-xs"><img src="{{URL::to('assets/img/logos/TNT_Studio.png')}}" class="img-responsive" alt="stardom logo"></div>
                </div>
                <div class="col-md-4">
                  <div class="pull-right text-right">
                    <h5> Sales Rep: <b class="text-primary">{{Auth::user()->fullName}}</b> </h5>
                    <p><strong>TNT Studio d.o.o</strong> <br>
                    Sv. Mateja 19 <br>
                    10000 Zagreb, Croatia <br>
                    <abbr title="Email">E:</abbr> <a href="mailto:info@tntstudio.hr">info@tntstudio.hr</a>
                    </p>
                  </div>
                </div>
              </div>
              <div class="row" id="invoice-info">
                <div class="col-md-4">
                  <div class="panel panel-alt">
                    <div class="panel-heading">
                      <div class="panel-title"> <i class="fa fa-user"></i> Bill To: </div>
                      <div class="panel-btns pull-right margin-left"> <span class="panel-title-sm"> Edit</span> </div>
                    </div>
                    <div class="panel-body">
                      <address>
                      <strong>Stefan Neubig</strong><br>
                      Oststraße 44<br>
                      74232 Abstatt, Germany<br>
                      <abbr title="Email">E:</abbr> mail@stefanneubig.com
                      </address>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-alt">
                    <div class="panel-heading">
                      <div class="panel-title"> <i class="glyphicons glyphicons-building"></i> Bank Info:</div>
                    </div>
                    <div class="panel-body">
                      <address>
                      <strong>IBAN</strong>: HR4523400091110572110<br>
                      <strong>SWIFT CODE</strong>: PBZGHR2X <br>
                      Privredna banka Zagreb d.d.<br>
                      Radnička cesta 50<br>
                      10000 Zagreb, Croatia <br>
                      <strong>Paypal</strong>: tntstudiohr@gmail.com
                      </address>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-alt">
                    <div class="panel-heading">
                      <div class="panel-title"> <i class="fa fa-info"></i> Invoice Details: </div>
                      <div class="panel-btns pull-right margin-left"> </div>
                    </div>
                    <div class="panel-body">
                      <ul class="list-unstyled">
                        <li> <b>Invoice #:</b> 58126332</li>
                        <li> <b>Invoice Date:</b> 10 Oct 2013</li>
                        <li> <b>Due Date:</b> 21 Dec 2013</li>
                        <li> <b>Terms:</b> Ten Forty</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" id="invoice-table">
                <div class="col-md-12">
                  <table class="table table-striped table-condensed">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Description</th>
                        <th style="width: 135px;">Quanitity</th>
                        <th>Rate</th>
                        <th class="text-right padding-right">Price</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>1</b></td>
                        <td>Developer Newsletter </td>
                        <td>Year Subscription X2</td>
                        <td>2</td>
                        <td>$12.99</td>
                        <td>$25.98</td>
                      </tr>
                      <tr>
                        <td><b>2</b></td>
                        <td>Resume Posting</td>
                        <td>Hand Posted Resume Applications (per posting)</td>
                        <td>4</td>
                        <td>$25.00</td>
                        <td>$100.00</td>
                      </tr>
                      <tr>
                        <td><b>3</b></td>
                        <td>Web Development</td>
                        <td>Worked on Design and Structure (per hour)</td>
                        <td>23</td>
                        <td>$30.00</td>
                        <td>$690.00</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row" id="invoice-footer">
                <div class="col-md-12"> <span> Thank you for your business.</span>
                  <div class="invoice-buttons">
                    <button class="btn btn-default margin-right" type="button"><i class="fa fa-print padding-right-sm"></i> Print Invoice</button>
                    <button class="btn btn-danger" type="button"><i class="fa fa-floppy-o padding-right-sm"></i> Save Invoice</button>
                  </div>
                  <div class="pull-right">
                    <table class="table" id="invoice-summary">
                      <thead>
                        <tr>
                          <th><b>Sub Total:</b></th>
                          <th>$230.00</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><b>Total:</b></td>
                          <td>$230.00</td>
                        </tr>
                        <tr>
                          <td><b>Payments</b></td>
                          <td>(-)0.00</td>
                        </tr>
                        <tr>
                          <td><b>Credits:</b></td>
                          <td>(-)0.00</td>
                        </tr>
                        <tr>
                          <td><b>Total</b></td>
                          <td>$230.00</td>
                        </tr>
                        <tr>
                          <td><b>Balance Due:</b></td>
                          <td>$230.00</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@stop