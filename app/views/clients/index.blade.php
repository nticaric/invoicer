@extends('layouts.main')

@section('container')
<div class="row" ng-app="ClientsApp">
        <!-- col -->
        <div class="col-sm-12">
            <!-- row-app -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9" ng-controller="ClientDetailController">

                    <div class="widget">
                        <div class="media widget-body">
                            <button class="pull-right btn btn-default btn-sm"><i class="fa fa-fw fa-edit"></i> Edit</button>
                            <img ng-src="{%client.picture%}" class="thumb pull-left animated fadeInDown" alt="" width="100" style="visibility: visible;">
                            <div class="media-body innerLR">
                                <h4 class="media-heading text-large text-primary">{%client.name%}</h4>
                                <p>{%client.email%} <br> {%client.street%} <br> {%client.zip%} {%client.city%} <br> {%client.country%}</p>
                            </div>
                        </div>
                    </div>

                    <div class="widget">
                        <div class="row row-merge margin-none ">
                            <div class="col-md-6">
                                <div class="innerAll text-center">
                                    <span class="text-large strong">9</span><br>
                                    Paid
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="innerAll text-center">
                                    <span class="text-large strong">1</span><br>
                                    Unpaid
                                </div>
                            </div>
                        </div>
                        <div class="row row-merge margin-none">
                            <div class="col-md-12">
                                <div class="innerAll text-center">
                                    <div class="media innerAll">
                                        <div class="pull-left">
                                            Payed Invoices
                                            <div class="strong">Till Today</div>
                                        </div>
                                        <div class="media-body innerAll">
                                            <div class="progress progress-small margin-none">
                                                <div class="progress-bar progress-bar-primary" style="width: 90%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget">

                        <div class="widget-head">
                            <h4 class="heading pull-left">Payment History</h4>
                        </div>
                        <div class="widget-body">
                            <ul class="timeline-widget list">
                                <li class="media">
                                  <div class="timeline-icon"><span class="glyphicons glyphicons-certificate text-blue2"></span> </div>
                                  <div class="media-body"> <b>Juicy Results</b> payed <b> $1500</b><span class="date"> - 4 hours ago</span> </div>
                                </li>
                                <li class="media">
                                  <div class="timeline-icon"><span class="glyphicons glyphicons-certificate text-blue2"></span> </div>
                                  <div class="media-body"> <b>Juicy Results</b> payed <b> $2800</b><span class="date"> - 12 days ago</span> </div>
                                </li>
                                <li class="media">
                                  <div class="timeline-icon"><span class="glyphicons glyphicons-certificate text-blue2"></span> </div>
                                  <div class="media-body"> <b>Juicy Results</b> payed <b> $7800</b><span class="date"> - 28.02.1986</span> </div>
                                </li>
                            </ul>
                        </div>

                    </div>          
                </div>
                <!-- // END col -->

                <!-- col -->
                <div class="col-lg-3">
                    <div class="widget" ng-controller="ClientsListController">

                        <div class="widget-head">
                            <h4 class="heading pull-left">Clients</h4>
                            <div class="pull-right">
                                <button class="btn btn-xs btn-default"
                                    data-template="angular/clients/aside/create.tpl.html"
                                    data-placement="right" 
                                    data-animation="am-slide-right" 
                                    bs-aside="aside"><i class="fa fa-plus"></i> Add client <i class="fa fa-user fa-fw"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                              
                        <div class="innerAll">
                            <div class="input-group input-group-sm">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Search</button>
                                </div>
                                <input type="text" ng-model="searchQuery" class="form-control" placeholder="Find a client ...">
                            </div>
                        </div>
                        <div class="widget-body padding-none">
                            <ul class="list-group list-group-1 borders-none margin-none">
                                <li ng-class="isActive(client)" class="list-group-item" ng-repeat="client in clients | filter:searchQuery">
                                    <div class="media innerAll">
                                        <button class="pull-right btn btn-primary btn-stroke btn-xs"><i class="fa fa-arrow-right"></i></button>
                                        <img ng-src="{%client.picture%}" alt="" width="35" class="pull-left thumb">
                                        <div class="media-body" ng-click="detailedView(client)">
                                            <h5 class="media-heading strong">{%client.name%}</h5>
                                            <ul class="list-unstyled">
                                                <li><i class="fa fa-envelope"></i> {%client.email%}</li>
                                                <li><i class="fa fa-map-marker"></i> {%client.street%}, {%client.zip%} {%client.city%}, {%client.country%}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- // End Widget-body-->
                    </div>
                    <!-- // END widget -->
                </div> 
                <!-- // END col -->
            </div>
            <!-- // END row -->
        </div>
        <!-- // END col-->
    </div>
@stop

@section('scripts')
<script>
    clientsApp = angular.module('ClientsApp', ['mgcrea.ngStrap']);
    clientsApp.config(function($interpolateProvider) {
      $interpolateProvider.startSymbol('{%');
      $interpolateProvider.endSymbol('%}');
    });

    clientsApp.service('clientService', function() {
      this.client = {};
    });

    clientsApp.controller('ClientsListController', ['$scope', '$http', 'clientService', 
        function($scope, $http, clientService) {
            $scope.clients = [];
            $http.get('/clients.json').then(function(response) {
                angular.copy(response.data[0], clientService.client);
                $scope.clients = response.data;
            })

            $scope.formData = {};
            $scope.processForm = function() {
                $http.post('/clients/add', $scope.formData)
                .success(function(data) {
                    $scope.clients.push(data);
                });
            }
            $scope.detailedView = function(client) {
                angular.copy(client, clientService.client);
            }
            $scope.isActive = function(client) {
                if(client.id == clientService.client.id)
                    return 'active';
                return '';
            }
    }]);

    clientsApp.controller('ClientDetailController', ['$scope', '$http', 'clientService',
        function($scope, $http, clientService) {
            $scope.client = clientService.client;
    }]);

    
</script>
@stop