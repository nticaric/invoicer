<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>TNT Studio Invoicer</title>
<meta name="keywords" content="Online Invoicer, Time Tracking" />
<meta name="description" content="TNT Studio Invoicer is an online invocing application and time tracker">
<meta name="author" content="Nenad Tičarić">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS  -->
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700">

<!-- Core CSS  -->
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/pages.css">
<link rel="stylesheet" type="text/css" href="assets/css/plugins.css">
<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">

<!-- Boxed-Layout CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/boxed.css">

<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.ico">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="minimal coming-soon-page"> 

<!-- Start: Main -->
<div id="main">
  <div class="container">
    <div class="row">
      <div id="page-logo"> <img src="assets/img/logos/TNT_Studio.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
      <div class="panel">
        <div class="panel-heading">
          <div class="panel-title text-center">TNT Studio Invoicer is coming to you soon! </div>
        </div>
        <div class="panel-body">
          <div id="counter"></div>
        </div>
        <div class="panel-footer">
          <p class="text-center margin-top"> Keep Updated. Enter your email address and we'll let you know as soon as it's ready.</p>
          <div class="form-group">
            <div class="col-xs-10 padding-none">
              <input type="text" class="form-control col-md-8" placeholder="Enter your email...">
            </div>
            <div class="col-xs-2 padding-none">
              <input class="submit btn btn-blue pull-right" type="submit" value="Sign Up" />
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: Main --> 

<!-- Core Javascript - via CDN --> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>  
<script type="text/javascript" src="assets/vendor/plugins/countdown/jquery.countdown.js"></script> 

<script type="text/javascript">
 jQuery(document).ready(function() {
    
    date_future = new Date(2014, 6-1, 1);
    date_now = new Date();

    seconds = Math.floor((date_future - (date_now))/1000);
    minutes = Math.floor(seconds/60);
    hours = Math.floor(minutes/60);
    days = Math.floor(hours/24);

    hours = hours-(days*24);
    minutes = minutes-(days*24*60)-(hours*60);
    seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);

    $('#counter').countdown({startTime: days + ":" + hours+":"+minutes+":"+seconds});
 });

</script>
</body>

</html>
