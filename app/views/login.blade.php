<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Invoicer - Login Page</title>
<meta name="keywords" content="Online Invoicer, Time Tracking" />
<meta name="description" content="TNT Studio Invoicer is an online invocing application and time tracker">
<meta name="author" content="Nenad Tičarić">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS  -->
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700">

<!-- Core CSS  -->
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/fonts/glyphicons_pro/glyphicons.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/pages.css">
<link rel="stylesheet" type="text/css" href="assets/fonts/zocial/css/zocial.css">
<link rel="stylesheet" type="text/css" href="assets/css/plugins.css">
<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">

<!-- Boxed-Layout CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/boxed.css">

<!-- Demonstration CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/demo.css">

<!-- Your Custom CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/custom.css">

<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.ico">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="login-page"> 

<!-- Start: Main -->
<div id="main">
  <div class="container">
    <div class="row">
      <div id="page-logo"></div>
    </div>
    <div class="row">
      <div class="panel">
        <div class="panel-heading">
          <div class="panel-title"> <span class="glyphicon glyphicon-lock"></span> Login </div>
        </div>
        <form class="cmxform" action="{{URL::to('login')}}" method="post">
          <div class="panel-body">
            <div class="login-avatar"> <img src="assets/img/logos/TNT_Studio.png" alt="avatar"> </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span>
                <input type="text" name="email" value="{{Input::old('email')}}" class="form-control phone"  autocomplete="off" placeholder="Email Address">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-link"></span> </span>
                <input type="text" name="password" class="form-control product"  autocomplete="off" placeholder="Password">
              </div>
            </div>
            @if($errors->has())
              @foreach($errors->all() as $message)
               <div class="alert alert-danger login-alert">{{ trans($message) }}</div>
              @endforeach
            @endif
            <a href="{{URL::to('login/google')}}" class="zocial googleplus">Login with Google+</a>
            <a href="{{URL::to('login/facebook')}}" class="zocial facebook">Login with Facebook</a>
          </div>
          <div class="panel-footer"> <span class="panel-title-sm pull-left" style="padding-top: 7px;"><a> Forgotten Password?</a></span>
            <div class="form-group margin-bottom-none">
              <input class="btn btn-primary pull-right" type="submit" value="Login" />
              <div class="clearfix"></div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End: Main --> 

<!-- Core Javascript - via CDN --> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> <!-- Theme Javascript --> 
<script type="text/javascript" src="assets/js/uniform.min.js"></script> 
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> 
<script type="text/javascript">

jQuery(document).ready(function() {

    // Init Theme Core    
    Core.init();   
    
});

</script>
</body>

</html>
