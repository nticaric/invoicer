<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>TNT Invoicer</title>
<meta name="keywords" content="Online Invoicer, Time Tracking" />
<meta name="description" content="TNT Studio Invoicer is an online invocing application and time tracker">
<meta name="author" content="Nenad Tičarić">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS  -->
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700">

<!-- Core CSS  -->
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/fonts/glyphicons_pro/glyphicons.min.css')}}">

<!-- Plugin CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/vendor/plugins/calendar/fullcalendar.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/vendor/plugins/datatables/css/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap-additions.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/angular-motion.min.css')}}">

<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/css/theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/css/pages.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/css/plugins.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/css/responsive.css')}}">

<!-- Boxed-Layout CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/boxed.css')}}">

<!-- Demonstration CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/demo.css')}}">

<!-- Your Custom CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/custom.css')}}">

<!-- Favicon -->
<link rel="shortcut icon" href="{{URL::to('assets/img/favicon.ico')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<?php $segment = Request::segment(1) ?>
<body class="dashboard {{$segment}}">

<!-- Start: Header -->
<header class="navbar navbar-fixed-top">
  <div class="pull-left"> <a class="navbar-brand" href="dashboard.html">
    <div class="navbar-logo"><h2>TNT Invoicer</h2></div>
    </a> </div>
  <div class="pull-right header-btns">
    <div class="messages-menu">
      <button type="button" class="btn btn-sm dropdown-animate" data-toggle="dropdown"> <span class="glyphicon glyphicon-comment"></span> <b>4</b> </button>
      <ul class="dropdown-menu checkbox-persist" role="menu">
        <li class="menu-arrow">
          <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Recent Messages <span class="pull-right glyphicons glyphicons-comment"></span></li>
        <li>
          <ul class="dropdown-items">
            <li>
              <div class="item-avatar"><img src="{{URL::to('assets/img/avatars/header/2.jpg')}}" class="img-responsive" alt="avatar" /></div>
              <div class="item-message"><b>Maggie</b> - <small class="text-muted">12 minutes ago</small><br />
                Great work Yesterday!</div>
            </li>
            <li>
              <div class="item-avatar"><img src="{{URL::to('assets/img/avatars/header/3.jpg')}}" class="img-responsive" alt="avatar" /></div>
              <div class="item-message"><b>Robert</b> - <small class="text-muted">3 hours ago</small><br />
                Is the Titan Project still on?</div>
            </li>
            <li>
              <div class="item-avatar"><img src="{{URL::to('assets/img/avatars/header/1.jpg')}}" class="img-responsive" alt="avatar" /></div>
              <div class="item-message"><b>Cynthia</b> - <small class="text-muted">14 hours ago</small><br />
                Don't forget about the staff meeting tomorrow</div>
            </li>
            <li>
              <div class="item-avatar"><img src="{{URL::to('assets/img/avatars/header/4.jpg')}}" class="img-responsive" alt="avatar" /></div>
              <div class="item-message"><b>Matt</b> - <small class="text-muted">2 days ago</small><br />
                Thor Project cancelled, Sorry.</div>
            </li>
          </ul>
        </li>
        <li class="dropdown-footer"><a href="messages.html">View All Messages <i class="fa fa-caret-right"></i> </a></li>
      </ul>
    </div>
    <div class="alerts-menu">
      <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="glyphicons glyphicons-bell"></span> <b>3</b> </button>
      <ul class="dropdown-menu checkbox-persist" role="menu">
        <li class="menu-arrow">
          <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Recent Alerts <span class="pull-right glyphicons glyphicons-bell"></span></li>
        <li>
          <ul class="dropdown-items">
            <li>
              <div class="item-icon"><i style="color: #0066ad;" class="fa fa-facebook"></i> </div>
              <div class="item-message"><a href="#">Facebook likes reached <b>8,200</b></a></div>
            </li>
            <li>
              <div class="item-icon"><i style="color: #5cb85c;" class="fa fa-check"></i> </div>
              <div class="item-message"><a href="#">Robert <b>completed task</b> - Client SEO Revamp</a></div>
            </li>
            <li>
              <div class="item-icon"><i style="color: #f0ad4e" class="fa fa-user"></i> </div>
              <div class="item-message"><a href="#"><b>Marko</b> logged 12 hours</a></div>
            </li>
          </ul>
        </li>
        <li class="dropdown-footer"><a href="#">View All Alerts <i class="fa fa-caret-right"></i> </a></li>
      </ul>
    </div>
    <div class="tasks-menu">
      <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="glyphicons glyphicons-tag"></span> <b>7</b> </button>
      <ul class="dropdown-menu dropdown-checklist checkbox-persist" role="menu">
        <li class="menu-arrow">
          <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Recent Tasks <span class="pull-right glyphicons glyphicons-tag"></span></li>
        <li>
          <ul class="dropdown-items">
            <li>
              <div class="item-icon"><i class="fa fa-pencil"></i> </div>
              <div class="item-message text-slash"><a>Try Clicking me!</a></div>
              <div class="item-label"><span class="label label-danger">Urgent</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
            <li>
              <div class="item-icon"><i class="fa fa-phone"></i> </div>
              <div class="item-message text-slash"><a>Contact Client and request Approval</a></div>
              <div class="item-label"><span class="label label-info">Normal</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
            <li>
              <div class="item-icon"><i class="fa fa-flask"></i> </div>
              <div class="item-message text-slash"><a>Find new Python Developer </a></div>
              <div class="item-label"><span class="label label-success">Completed</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
            <li>
              <div class="item-icon"><i class="fa fa-facebook"></i> </div>
              <div class="item-message text-slash"><a>Update Facebook Page with Halloween Pictures</a></div>
              <div class="item-label"><span class="label label-primary">In Progress</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
            <li>
              <div class="item-icon"><i class="fa fa-group"></i> </div>
              <div class="item-message text-slash"><a>Organize next Staff Meeting</a></div>
              <div class="item-label"><span class="label label-success">Completed</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
            <li>
              <div class="item-icon"><i class="fa fa-plane"></i> </div>
              <div class="item-message text-slash"><a>Interview new applicant</a></div>
              <div class="item-label"><span class="label label-warning">Moderate</span></div>
              <div class="item-checkbox">
                <input class="checkbox row-checkbox" type="checkbox">
              </div>
            </li>
          </ul>
        </li>
        <li class="dropdown-footer"><a href="#">View All Tasks <i class="fa fa-caret-right"></i> </a></li>
      </ul>
    </div>
    <div class="btn-group user-menu">
      <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="glyphicons glyphicons-user"></span> <b>{{Auth::user()->fullName}}</b> </button>
      <button type="button" class="btn btn-sm dropdown-toggle padding-none" data-toggle="dropdown"> 
        <img src="@if(Auth::user()->avatar){{Auth::user()->avatar}}@elseassets/img/avatars/header/header-login.png@endif" alt="user avatar" width="28" height="28" /> </button>
      <ul class="dropdown-menu checkbox-persist" role="menu">
        <li class="menu-arrow">
          <div class="menu-arrow-up"></div>
        </li>
        <li class="dropdown-header">Your Account <span class="pull-right glyphicons glyphicons-user"></span></li>
        <li>
          <ul class="dropdown-items">
            <li>
              <div class="item-icon"><i class="fa fa-envelope-o"></i> </div>
              <a class="item-message" href="messages.html">Messages</a> </li>
            <li>
              <div class="item-icon"><i class="fa fa-calendar"></i> </div>
              <a class="item-message" href="calendar.html">Calendar</a> </li>
            <li class="border-bottom-none">
              <div class="item-icon"><i class="fa fa-cog"></i> </div>
              <a class="item-message" href="customizer.html">Settings</a> </li>
            <li class="padding-none">
              <div class="dropdown-lockout"><i class="fa fa-lock"></i> <a href="screen-lock.html">Screen Lock</a></div>
              <div class="dropdown-signout"><i class="fa fa-sign-out"></i> <a href="{{URL::to('logout')}}">Sign Out</a></div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</header>
<!-- End: Header --> 
<!-- Start: Main -->
<div id="main"> 
  <!-- Start: Sidebar -->
  <aside id="sidebar">
    <div id="sidebar-search">
      <form role="search">
        <input type="text" class="search-bar form-control" placeholder="Search">
        <i class="fa fa-search field-icon-right"></i>
        <button type="submit" class="btn btn-default hidden"></button>
      </form>
      <div class="sidebar-toggle"> <span class="glyphicon glyphicon-resize-horizontal"></span> </div>
    </div>
    <div id="sidebar-menu">
      <ul class="nav sidebar-nav">
        <li @if(Request::segment(1) == '') class="active" @endif><a href="{{URL::to('/')}}"><span class="glyphicons glyphicons-star"></span><span class="sidebar-title">Dashboard</span></a> </li>
        <li @if(Request::segment(1) == 'invoices') class="active" @endif><a class="accordion-toggle" href="#sideEight"><span class="glyphicons glyphicons-notes_2"></span><span class="sidebar-title">Invoices</span><span class="caret"></span></a>
          <ul id="sideEight" class="nav sub-nav">
            <li class="active"><a href="{{URL::to('invoices/new')}}"><span class="glyphicons glyphicons-file"></span>New Invoice</a></li>
            <li><a href="#"><span class="glyphicons glyphicons-stats"></span> Statistics</a></li>
          </ul>
        </li>
        <li @if(Request::segment(1) == 'clients') class="active" @endif><a href="{{URL::to('/clients')}}"><span class="glyphicons glyphicons-group"></span><span class="sidebar-title">Clients</span></a> </li>
        <li @if(Request::segment(1) == 'estimates') class="active" @endif><a href="{{URL::to('/estimates')}}"><span class="glyphicons glyphicons-table"></span><span class="sidebar-title">Estimates</span></a> </li>
        <li @if(Request::segment(1) == 'expenses') class="active" @endif><a href="{{URL::to('/expenses')}}"><span class="glyphicons glyphicons-cart_in"></span><span class="sidebar-title">Expenses</span></a> </li>
        <li @if(Request::segment(1) == 'timetracking') class="active" @endif><a href="{{URL::to('/timetracking')}}"><span class="glyphicons glyphicons-stopwatch"></span><span class="sidebar-title">Time Tracking</span></a> </li>
        <li @if(Request::segment(1) == 'reports') class="active" @endif><a href="{{URL::to('/reports')}}"><span class="glyphicons glyphicons-charts"></span><span class="sidebar-title">Reports</span></a> </li>
        
      </ul>
    </div>
  </aside>
  <!-- End: Sidebar --> 
  <!-- Start: Content -->
  <section id="content">
    <div id="topbar">
      <ol class="breadcrumb">
        <li><a href="dashboard.html"><span class="glyphicon glyphicon-home"></span></a></li>
        <li><a href="index.html">Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </div>
    <div class="container">
      @yield('container')
    </div>
  </section>
  <!-- End: Content --> 
</div>
<!-- End: Main --> 

<!-- Core Javascript - via CDN --> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> <!-- Plugins - Via CDN --> 

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular-animate.min.js"></script>
<script src="{{URL::to('bower_components/angular-strap/dist/angular-strap.min.js')}}"></script>
<script src="{{URL::to('bower_components/angular-strap/dist/angular-strap.tpl.min.js')}}"></script>

<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/flot/0.8.1/jquery.flot.min.js"></script> 
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script> 
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fullcalendar/1.6.4/fullcalendar.min.js"></script> 

<!-- Plugins - Via Local Storage
<script type="text/javascript" src="assets/vendor/plugins/jqueryflot/jquery.flot.min"></script>
<script type="text/javascript" src="assets/vendor/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/calendar/fullcalendar.min.js"></script>
--> 

<!-- Plugins --> 
<script type="text/javascript" src="{{URL::to('assets/vendor/plugins/calendar/gcal.js')}}"></script><!-- Calendar Addon --> 
<script type="text/javascript" src="{{URL::to('assets/vendor/plugins/jqueryflot/jquery.flot.resize.min.js')}}"></script><!-- Flot Charts Addon --> 
<script type="text/javascript" src="{{URL::to('assets/vendor/plugins/datatables/js/datatables.js')}}"></script><!-- Datatable Bootstrap Addon --> 

<!-- Theme Javascript --> 
<script type="text/javascript" src="{{URL::to('assets/js/uniform.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/js/main.js')}}"></script> 
<!--<script type="text/javascript" src="assets/js/plugins.js"></script>--> 
<script type="text/javascript" src="{{URL::to('assets/js/custom.js')}}"></script> 
@yield('scripts')
<script type="text/javascript">
  jQuery(document).ready(function() {
      
    // Init Theme Core    
    Core.init();

    // Init Datatables
    $('#datatable, #datatable_2').dataTable( {
      "bSort": true,
      "bPaginate": false,
      "bLengthChange": false,
      "bFilter": false,
      "bInfo": false,
      "bAutoWidth": false,
      "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ -1 ] }]
    });
    
    // Init Sparkline Plugin
    var runSparkline = function () {
        
        // Define Sparklines values
        var Values1 = [0,1,5,2,4,3,5,8,7];
        var Values2 = [3,2,3,1,0,2,5,6,4];
        var Values3 = [6,4,5,3,4,2,1,2,3];
        var Values4 = [2,1,2,3,2,4,2,1,0];

        // Pass values, define options, and create chart
        $('.sparkbar1').sparkline(Values1, {type: "bar",
                barColor: "#428bca",
                barWidth: "8",
                height: "35"} 
        );
        $('.sparkbar2').sparkline(Values2, {type: "bar",
                barColor: "#5cb85c",
                barWidth: "8",
                height: "35"} 
        );
        $('.sparkbar3').sparkline(Values3, {type: "bar",
                barColor: "#5bc0de",
                barWidth: "8",
                height: "35"} 
        );
        $('.sparkbar4').sparkline(Values4, {type: "bar",
                barColor: "#777777",
                barWidth: "8",
                height: "35"} 
        );
        
        // Quick function to animate the appearance of Sparklines
        // Only ran on Sparkline elements which have the 
        // ".sparkline-delay" class and a set data-delay attr
        (function() {
            $('.sparkline-delay').each(function() {
                var This = $(this)
                var delayTime = $(this).data('delay');
    
                $(This).children('canvas').css({"height": "0", "vertical-align": "bottom"});
                var delayCharts = setTimeout(function () {
                      $(This).children('canvas').animate({height: 35}, 1800);
                }, delayTime); 
            });
        })();       
    }

    // Init Calendar Plugin
    var runFullCalendar = function () {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        // Define Calendar options and events
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 9),
                    color: '#008aaf '
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-3)
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+3, 16, 0),
                    allDay: false
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+10, 16, 0),
                    allDay: false
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    color: '#0070ab'
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    color: '#0070ab'
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Mandatory!',
                    start: new Date(y, m, 22),
                    color: '#d10011'
                }
            ]
        });
    }

    // Init Flot Charts Plugin
    var runFlotCharts = function () {
        
    // Define chart clolors ( add more colors if you want or flot will do it automatically
    var chartColours = ['#5bc0de'];

    // Generate random number for charts
    randNum = function(){return (Math.floor( Math.random()* (1+40-20) ) ) + 20;}

    // Check if element exist and draw auto updating chart
    if($(".chart-updating").length) {
        $(function () {
            // We use an inline data source in the example, usually data would
            // be fetched from a server
            var data = [], totalPoints = 50;
            function getRandomData() {
                if (data.length > 0)
                    data = data.slice(1);

                while (data.length < totalPoints) {
                    var prev = data.length > 0 ? data[data.length - 1] : 50;
                    var y = prev + Math.random() * 10 - 5;
                    if (y < 0)
                        y = 0;
                    if (y > 100)
                        y = 100;
                    data.push(y);
                }

                var res = [];
                for (var i = 0; i < data.length; ++i)
                    res.push([i, data[i]])
                return res;
            }

            var updateInterval = 750;

            var options = {
                series: { 
                    shadowSize: 0, // drawing is faster without shadows
                    lines: {
                        show: true,
                        fill: true,
                        fillColor: { colors: ['rgba(47, 137, 214, 0.4)', 'rgba(98, 174, 239, 0.3)'] },
                        lineWidth: 1,
                        steps: false
                    },
                    points: {
                        show: false,
                        radius: 2.5,
                        symbol: "circle",
                        lineWidth: 2.5
                    }
                },
                grid: {
                    show: true,
                    aboveData: false,
                    color: "#3f3f3f",
                    labelMargin: 5,
                    axisMargin: 0, 
                    borderWidth: 0,
                    borderColor:null,
                    minBorderMargin: 5 ,
                    clickable: true, 
                    hoverable: true,
                    autoHighlight: false,
                    mouseActiveRadius: 20
                }, 
                colors: chartColours,
                yaxis: { 
                     min: 0,
                     max: 100,
                     font: {size: 11,   color: "#888888"}
                },
                xaxis: { 
                     show: true,
                     font: {size: 11,   color: "#888888"}
                }
            };
            var plot = $.plot($(".chart-updating"), [ getRandomData() ], options);

            function update() {
                plot.setData([ getRandomData() ]);
                // since the axes don't change, we don't need to call plot.setupGrid()
                plot.draw();
                
                setTimeout(update, updateInterval);
            }
            update();
        });
      }
  }
     
  runSparkline();
  runFullCalendar();      
  runFlotCharts();
  
  // Example animations ran on Header Buttons - For Demo Purposes
  $('.header-btns > div').on('show.bs.dropdown', function() {
      $(this).children('.dropdown-menu').addClass('animated animated-short flipInY');
  });
  $('.header-btns > div').on('hide.bs.dropdown', function() {
      $(this).children('.dropdown-menu').removeClass('animated flipInY');
  });
              

});
</script>
</body>
</html>